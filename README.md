# RITVAL's final project: Board game manager

### Description of project:
* A GUI written in Python using the Tkinter library.
* The program allows you to create, edit, search and save different board game collections stored in JSON format through an interactive interface.
* An example collection is provided but feel free to create your very own!

### Some features:
* Runs on any OS with a standardized theme using ttk.
* The ability to create new JSON files directly through the GUI and switch game collections using the file explorer.
* Creates a data directory and an empty games collection for you in case you managed to lose the example that is provided.
* Prompt for unsaved changes when exiting the program or changing collection.
* Collecting data from the selected Treeview row and displaying it in the Entry boxes for quick editing.
  
### How to run:
* Run the program from 'app.py' in the terminal of your choice and be amazed!

### Dependencies:
* Python 3.1+ (3.9.X recommended)
* Using Tkinter (8.5+ required, using ttk).

### Not working?
* Check your tkinter version by running "python -m tkinter" or "python3 -m tkinter" in the console of your choice, ensure your version is up to date.
* Ensure your Python version is up to date by running "python --version" or "python3 --version".

In Python 3.9.X the correct version of Tkinter is a part of the standard library if you installed it through python.org. If you wish to run the program with an older version of Python please see the instructions below taken from tkdocs.com if you encounter any issues:

<em>"The Tkinter module is included with core Python, of course, but you'll need a version of Tcl/Tk on your system to compile it against. Do yourself a huge favor and get the most recent version.</em>

<em>Whatever you do, do not rely on the Tk versions included in macOS! Older versions included Tk 8.4.x. Even more recent macOS versions include an early 8.5 version (8.5.9, released in 2010), which has several serious bugs that are easily triggered by Tkinter."</em>

https://www.activestate.com/products/tcl/downloads/