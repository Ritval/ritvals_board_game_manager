from copy import deepcopy
from file_handler import FileHandler
from root_frame import root_frame
from runtime_logger import RuntimeLogger
from tkinter import *
from tkinter import filedialog, messagebox, ttk
from window_manager import ButtonWindow, EntryWindow, MenuBar, TreeViewWindow


class App():
    def __init__(self):
        self.file_handler = FileHandler()
        self.tree = TreeViewWindow()
        self.entry = EntryWindow()
        self.buttons = ButtonWindow()
        self.logger = RuntimeLogger()
        self.menu = MenuBar()
        self.style = ttk.Style()

        self.style.theme_use('clam')
        self.file_handler.create_initial_json()
        self.logger.old_content_log = self.file_handler.get_json_file_data(self.file_handler.current_collection_path)
        self.logger.runtime_log = deepcopy(self.logger.old_content_log)
        
        self.tree.bind("<ButtonRelease-1>", self.display_row_in_entries)
        self.tree.bind("<ButtonPress-1>", self.disable_separator_resizing)

        self.buttons.add.config(command=self.add)
        self.buttons.delete.config(command=self.delete)
        self.buttons.search.config(command=self.search)
        self.buttons.update_btn.config(command=self.update)
        self.buttons.clear.config(command=self.entry.clear_entries)
        self.buttons.exit.config(command=self.exit)
        root_frame.root.protocol("WM_DELETE_WINDOW", self.exit)

        self.menu.file_menu.add_command(label="New", command=self.new)
        self.menu.file_menu.add_command(label="Open", command=self.open)
        self.menu.file_menu.add_command(label="Save", command=self.save)
        
        root_frame.pack()
        self.buttons.pack(side=LEFT,fill=Y)
        self.entry.pack(side=TOP)
        self.tree.tree_window.pack(side=BOTTOM)
        self.tree.pack()

        root_frame.root.eval('tk::PlaceWindow . center')
        self.reload_tree(self.logger.runtime_log)

    def add(self):
        if self.entry.get_if_valid_values():
            if self.entry.get_if_complete_entry():
                entries = self.entry.get_entries()
                if not self.logger.get_if_title_exists(entries[0]):
                    self.logger.append_data_logger(entries)
                    self.reload_tree(self.logger.runtime_log)
                else:
                    messagebox.showwarning("Error", "Invalid entry.\nGame name already exists.")
            else:
                messagebox.showwarning("Error", "Invalid entry.\nAll values except rating are mandatory.")
        else:
            messagebox.showwarning("Error", "Invalid input.\nUnable to add game.")

    def delete(self):
        if not self.logger.get_if_log_is_empty():
            if not self.tree.selected_row == "":
                entries = self.entry.get_entries()
                self.logger.delete_by_title(entries[0])
                self.tree.selected_row = ""
                self.reload_tree(self.logger.runtime_log)
            else:
                messagebox.showwarning("Error", "No row is currently selected.")
        else:
            messagebox.showinfo("Info", "Your collection is currently empty.\nNothing to delete.")
    
    def search(self):
        if not self.logger.get_if_log_is_empty():
            if self.entry.get_if_valid_values():
                entries = self.entry.get_entries()
                search_result = self.logger.get_filtered_log(entries)
                self.reload_tree(search_result)
            else:
                messagebox.showwarning("Error", "Invalid input.\nUnable to search.")
        else:
            messagebox.showinfo("Info", "Your collection is currently empty.\nNothing to search for.")
            
    def update(self):
        if self.tree.selected_row in self.tree.get_children():
            if self.entry.get_if_complete_entry():
                if self.entry.get_if_valid_values():
                    entries = self.entry.get_entries()
                    old_title = self.tree.item(self.tree.selected_row)['values'][0]
                    self.logger.update_game_info(old_title, entries)
                    self.reload_tree(self.logger.runtime_log)
                else:
                    messagebox.showwarning("Error", "Unable to update.\nInvalid input.")
            else:
                messagebox.showwarning("Error", "Unable to update.\nAll values except rating are mandatory.")
        else:
            messagebox.showwarning("Error", "No game selected.\nUnable to update.")
    
    def new(self):
        try:
            new_file_path = filedialog.asksaveasfilename(
                filetypes=[("JSON Files","*.json")], 
                initialdir=self.file_handler.DATA_DIRECTORY_PATH, defaultextension='.json'
            )
            self.file_handler.create_new_json(new_file_path)
            self.logger.runtime_log = self.file_handler.get_json_file_data(new_file_path)
            self.file_handler.current_collection_path = new_file_path
            self.reload_tree(self.logger.runtime_log)
        except FileNotFoundError:
            messagebox.showinfo("Info", "No new collection has been created.\nReturning to your current collection.")
        except PermissionError:
            messagebox.showwarning("Error", "Permission denied.\nTry running the program as administrator.")   

    def open(self):
        if self.logger.get_if_unsaved_changes():
            user_choice = messagebox.askyesno("Hmm...", "You have unsaved changes.\nAre you sure you want to open a new collection?")
            if not user_choice:
                return
        open_file_path = filedialog.askopenfilename(filetypes=[("JSON Files","*.json")], initialdir=self.file_handler.DATA_DIRECTORY_PATH)
        if self.file_handler.get_if_file_exists(open_file_path):
            self.logger.runtime_log = self.file_handler.get_json_file_data(open_file_path)
            self.file_handler.current_collection_path = open_file_path
            self.reload_tree(self.logger.runtime_log)

    def save(self):
        if self.file_handler.write_to_json_file_and_return_confirmation(self.logger.runtime_log, self.file_handler.current_collection_path):
            messagebox.showinfo("Success!", "Your games have been saved!")
            self.logger.old_content_log = deepcopy(self.logger.runtime_log)
        else:
            messagebox.showerror("Error", "It seems like your 'data' directory is missing."
            + "\nTry restarting the program as administrator to generate one or manually create an empty directory called 'data'.")

    def exit(self):
        try:
            if not self.logger.get_if_unsaved_changes():
                root_frame.root.destroy()
            else:
                user_choice = messagebox.askyesno("Hmm...", "You have unsaved changes.\nAre you sure you want to Exit?")
                if user_choice:
                    root_frame.root.destroy()
        except FileNotFoundError:
            exit()
    
    def display_row_in_entries(self, event):
        self.tree.selected_row = self.tree.focus()
        row_item = self.tree.item(self.tree.selected_row)
        self.entry.set_entries_text(row_item['values'])
    
    def disable_separator_resizing(self, event):
        if self.tree.identify_region(event.x, event.y) == "separator":
            return "break"

    def reload_tree(self, log_to_display):
        self.tree.delete(*self.tree.get_children())
        if not self.logger.get_if_log_is_empty():
            try:
                for index, dict in enumerate(log_to_display['games']):
                    self.tree.insert(index='end', parent='', iid=index, values=(dict['title'], dict['players'], dict['age'], dict['duration'], dict['rating']))
            except KeyError:
                messagebox.showinfo("Info", "Unable to fully load your current collection due to invalid values.\nOpen or create a new one!")
        else:
            messagebox.showinfo("Info", "Your collection is loaded but currently empty.\nAdd some games!")


if __name__ == "__main__":
    try:
        gui = App()
        root_frame.mainloop()
    except Exception as e:
        print(f"Something went wrong. Please pass this error to the maintainer.")
        print(e)