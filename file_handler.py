import json
import os

class FileHandler:
    def __init__(self):
        _dirname = os.path.dirname(__file__)
        self.DATA_DIRECTORY_PATH = os.path.join(_dirname, 'data')
        self._INITIAL_JSON_PATH = os.path.join(self.DATA_DIRECTORY_PATH, 'games.json')
        self.current_collection_path = self._INITIAL_JSON_PATH
        os.makedirs(self.DATA_DIRECTORY_PATH, exist_ok=True)

    def get_json_file_data(self, file_path):
        try:
            with open(file_path) as f:
                data = json.load(f)
            return data
        except json.decoder.JSONDecodeError:
            # File is empty or invalid JSON-format. Return empty collection.
            return {"games": []}

    def write_to_json_file_and_return_confirmation(self, runtime_log, file_path):
        try:
            with open(file_path, 'w') as f:
                json.dump(runtime_log, f, indent=4)
                return True
        except FileNotFoundError:
            return False

    def create_initial_json(self):
        if not self.get_if_file_exists(self.current_collection_path):
            content = {"games": []}
            with open(self._INITIAL_JSON_PATH, 'w') as f:
                json.dump(content, f, indent=4)

    def create_new_json(self, file_path):
        content = {"games": []}
        with open(file_path, 'w') as f:
            json.dump(content, f, indent=4)

    def get_if_file_exists(self, file_path):
        return os.path.isfile(file_path)