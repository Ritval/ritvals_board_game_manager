from tkinter import *
from tkinter import ttk
import os

class MainFrame(ttk.Frame):
    # Singleton
    _dirname = os.path.dirname(__file__)
    _ICON_DIRECTORY = os.path.join(_dirname, 'assets', 'icon.png')
    def __init__(self):
        self.root = Tk()
        self.root.title("Board game manager")
        icon = PhotoImage(file=self._ICON_DIRECTORY)
        self.root.iconphoto(False, icon)
        self.root.resizable(height=False, width=False)
        super().__init__(self.root, width=510, height=510)
        
root_frame = MainFrame()

