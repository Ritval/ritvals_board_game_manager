from copy import deepcopy

class RuntimeLogger:
    def __init__(self):
        self.old_content_log = {}
        self.runtime_log = {}
            
    def get_if_title_exists(self, searched_value):
        for item in self.runtime_log['games']:
            if item['title'] == searched_value:
                return True
        return False

    def get_if_log_is_empty(self):
        try:
            if len(self.runtime_log['games']) == 0:
                return True
            return False
        except TypeError:
            return True
        except KeyError:
            return True

    def delete_by_title(self, title):
        for index, item in enumerate(self.runtime_log['games']):
            if item['title'] == title:
                del self.runtime_log['games'][index]
    
    def update_game_info(self, old_title, entries):
        for index, game in enumerate(self.runtime_log['games']):
            if game['title'] == old_title:
                self.runtime_log['games'][index]['title'] = entries[0]
                self.runtime_log['games'][index]['players'] = entries[1]
                self.runtime_log['games'][index]['age'] = entries[2]
                self.runtime_log['games'][index]['duration'] = entries[3]
                self.runtime_log['games'][index]['rating'] = entries[4]

    def get_if_unsaved_changes(self):
        return self.old_content_log != self.runtime_log
    
    def append_data_logger(self, entries):
        self.runtime_log['games'].append({"title" : entries[0], "players" : entries[1], "age" : entries[2], "duration" : entries[3], "rating" : entries[4]})
                
    def get_filtered_log(self, values):
        filtered_list = deepcopy(self.runtime_log)
        if len(values[0]) != 0:
            filtered_list = self._filter_titles(values[0], filtered_list)
        if len(values[1]) != 0:
            filtered_list = self._filter_players_age_or_duration('players', values[1], filtered_list)
        if len(values[2]) != 0:
            filtered_list = self._filter_players_age_or_duration('age', values[2], filtered_list)
        if len(values[3]) != 0:
            filtered_list = self._filter_players_age_or_duration('duration', values[3], filtered_list)
        if len(values[4]) != 0:
            filtered_list = self._filter_rating(values[4], filtered_list)

        return filtered_list

    def _filter_titles(self, value, list):
        for item in self.runtime_log['games']:
            try:
                if not item['title'].lower().startswith(value.lower()):
                    list['games'].remove(item)
            except ValueError:
                continue
        return list

    def _filter_rating(self, value,list):
        for item in self.runtime_log['games']:
            if item['rating'] == '':
                try:
                    list['games'].remove(item)
                except ValueError:
                    continue
            else:
                try:
                    if int(item['rating']) != int(value):
                        list['games'].remove(item)
                except ValueError:
                    continue
        return list

    def _filter_players_age_or_duration(self, key, value, list):
        for item in self.runtime_log['games']:
            try:
                if int(item[key]) != int(value):
                    list['games'].remove(item)
            except ValueError:
                    continue
        return list