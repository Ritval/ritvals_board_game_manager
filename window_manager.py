from root_frame import root_frame
from tkinter import *
from tkinter import ttk
            

class TreeViewWindow(ttk.Treeview):
    def __init__(self):
        self.tree_window = ttk.Frame(root_frame)
        self.tree_scroller = ttk.Scrollbar(self.tree_window, orient=VERTICAL, command=self.yview)
        self.tree_scroller.pack(side=RIGHT, fill=Y)
        super().__init__(
            self.tree_window, show="headings", height=14, yscrollcommand=self.tree_scroller.set, columns=("title", "players", "age", "duration", "rating")
        )
        self.selected_row = ""
        self.heading("title", text="Title:")
        self.heading("players", text="Players:")
        self.heading("age", text="Age:")
        self.heading("duration", text="Duration:")
        self.heading("rating", text="Rating:")

        self.column("title", width=120, anchor=W)
        self.column("players", width=100, anchor=W)
        self.column("age", width=100, anchor=W)
        self.column("duration", width=100, anchor=W)
        self.column("rating", width=80, anchor=W)
        

class MenuBar(Menu):
    def __init__(self):
        super().__init__(root_frame)
        root_frame.root.config(menu=self)

        self.file_menu = Menu(self, tearoff=0)
        self.add_cascade(label="File",menu=self.file_menu)


class ButtonWindow(ttk.Frame):
    def __init__(self):
        super().__init__(root_frame)
        
        self.add = ttk.Button(self, text="Add")
        self.add.pack(side=TOP, ipady=8, ipadx=2)

        self.delete = ttk.Button(self, text="Delete")
        self.delete.pack(side=TOP, ipady=8, ipadx=2)

        self.search = ttk.Button(self, text="Search")
        self.search.pack(side=TOP, ipady=8, ipadx=2)

        self.update_btn = ttk.Button(self, text="Update")
        self.update_btn.pack(side=TOP, ipady=8, ipadx=2)

        self.clear = ttk.Button(self, text="Clear")
        self.clear.pack(side=TOP, ipady=8, ipadx=2)

        self.exit = ttk.Button(self, text="Exit")
        self.exit.pack(side=BOTTOM, ipady=8, ipadx=2)


class EntryWindow(ttk.Frame):
    def __init__(self):
        super().__init__(root_frame)

        self.title = StringVar()
        self.players = StringVar()
        self.age = StringVar()
        self.duration = StringVar()
        self.rating = StringVar()

        title_label = ttk.Label(self, padding=5, text="Game title: ")
        title_label.grid(row=0,column=0)
        self.title_entry = ttk.Entry(self, textvariable=self.title)
        self.title_entry.grid(row=0,column=1)
        
        players_label = ttk.Label(self, padding=5, text="Amount of players: ")
        players_label.grid(row=1,column=0)
        self.players_entry = ttk.Entry(self, textvariable=self.players)
        self.players_entry.grid(row=1,column=1)

        age_label = ttk.Label(self, padding=5, text="Recommended age: ")
        age_label.grid(row=2,column=0)
        self.age_entry = ttk.Entry(self, textvariable=self.age)
        self.age_entry.grid(row=2,column=1)

        duration_label = ttk.Label(self, padding=5, text="Game duration: ")
        duration_label.grid(row=3,column=0)
        self.duration_entry = ttk.Entry(self, textvariable=self.duration)
        self.duration_entry.grid(row=3,column=1)

        rating_label = ttk.Label(self, padding=5, text="Rating:")
        rating_label.grid(row=4, column=0)
        drop_options= ["","1","2","3","4","5"]
        self.rating_box = ttk.Combobox(self,width=6,textvariable=self.rating, values=drop_options, state="readonly")
        self.rating_box.grid(row=4,column=1, sticky=W)

    def clear_entries(self):
        self.title_entry.delete(0, END)
        self.players_entry.delete(0, END)
        self.age_entry.delete(0, END)
        self.duration_entry.delete(0, END)
        self.rating_box.set("")

    def get_entries(self):
        title = self.title.get().title()
        players = self.players.get()
        age = self.age.get()
        duration = self.duration.get()
        rating = self.rating.get()
        return title, players, age, duration, rating

    def set_entries_text(self,game_values):
        try:
            self.clear_entries()
            self.title_entry.insert(0,game_values[0])
            self.players_entry.insert(0,game_values[1])
            self.age_entry.insert(0,game_values[2])
            self.duration_entry.insert(0,game_values[3])
            self.rating_box.set(game_values[4])
        except IndexError:
            pass
    
    def get_if_valid_values(self):
        entries = self.get_entries()
        if len(entries[1]) != 0:
            try:
                int(entries[1])
            except ValueError:
                return False
        if len(entries[2]) != 0:
            try:
                int(entries[2])
            except ValueError:
                return False
        if len(entries[3]) != 0:
            try:
                int(entries[3])
            except ValueError:
                return False
        return True
    
    def get_if_complete_entry(self):
        entries = self.get_entries()
        if len(entries[0]) == 0:
            return False
        if len(entries[1]) == 0:
            return False
        if len(entries[2]) == 0:
            return False
        if len(entries[3]) == 0:
            return False
        return True   